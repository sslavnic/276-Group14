class AddGoogleAccountToClubs < ActiveRecord::Migration
  def change
    add_column :clubs, :google_account, :string
  end
end
