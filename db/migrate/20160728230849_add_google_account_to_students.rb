class AddGoogleAccountToStudents < ActiveRecord::Migration
  def change
    add_column :students, :google_account, :string
  end
end
