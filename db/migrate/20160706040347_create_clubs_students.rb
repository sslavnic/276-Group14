class CreateClubsStudents < ActiveRecord::Migration
  def change
    create_table :clubs_students, :id => false do |t|
    	t.belongs_to :club
    	t.belongs_to :student
    end
  end
end
