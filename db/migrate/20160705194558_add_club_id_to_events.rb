class AddClubIdToEvents < ActiveRecord::Migration
  def change
    add_reference :events, :club
  end
end
