class CreateTokens < ActiveRecord::Migration
  def change
    create_table :tokens do |t|
      t.string :access_token
      t.string :refresh_token
      t.datetime :expires_at
      t.references :student, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
