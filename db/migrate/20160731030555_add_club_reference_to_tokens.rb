class AddClubReferenceToTokens < ActiveRecord::Migration
  def change
    add_reference :tokens, :club, index: true, foreign_key: true
  end
end
