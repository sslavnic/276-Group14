class AddGoogleEventIdToStudents < ActiveRecord::Migration
  def change
    add_column :students, :google_event_id, :string
  end
end
