class AddStudentToEvents < ActiveRecord::Migration
  def change
    add_reference :events, :student, index: true, foreign_key: true
  end
end
