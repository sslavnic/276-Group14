# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160802022448) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "clubs", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "admin_id"
    t.boolean  "choice1"
    t.boolean  "choice2"
    t.boolean  "choice3"
    t.boolean  "choice4"
    t.boolean  "choice5"
    t.boolean  "choice6"
    t.boolean  "choice7"
    t.boolean  "choice8"
    t.boolean  "choice9"
    t.boolean  "choice10"
    t.string   "website"
    t.string   "google_account"
    t.string   "SunDesc"
    t.string   "MonDesc"
    t.string   "TueDesc"
    t.string   "WedDesc"
    t.string   "ThuDesc"
    t.string   "FriDesc"
    t.string   "SatDesc"
    t.string   "SunTime"
    t.string   "MonTime"
    t.string   "TueTime"
    t.string   "WedTime"
    t.string   "ThuTime"
    t.string   "FriTime"
    t.string   "SatTime"
    t.string   "SunPlace"
    t.string   "MonPlace"
    t.string   "TuePlace"
    t.string   "WedPlace"
    t.string   "ThuPlace"
    t.string   "FriPlace"
    t.string   "SatPlace"
    t.string   "insta"
    t.string   "twitt"
  end

  create_table "clubs_students", id: false, force: :cascade do |t|
    t.integer "club_id"
    t.integer "student_id"
  end

  create_table "events", force: :cascade do |t|
    t.string   "name"
    t.datetime "start_time"
    t.string   "place"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "club_id"
    t.datetime "end_time"
    t.string   "google_id"
    t.integer  "student_id"
  end

  add_index "events", ["student_id"], name: "index_events_on_student_id", using: :btree

  create_table "students", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password"
    t.string   "password_confirmation"
    t.string   "password_digest"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "activation_digest"
    t.boolean  "activated",             default: false
    t.datetime "activated_at"
    t.string   "studenttype"
    t.string   "google_account"
    t.string   "google_event_id"
  end

  create_table "tokens", force: :cascade do |t|
    t.string   "access_token"
    t.string   "refresh_token"
    t.datetime "expires_at"
    t.integer  "student_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "club_id"
  end

  add_index "tokens", ["club_id"], name: "index_tokens_on_club_id", using: :btree
  add_index "tokens", ["student_id"], name: "index_tokens_on_student_id", using: :btree

  add_foreign_key "events", "students"
  add_foreign_key "tokens", "clubs"
  add_foreign_key "tokens", "students"
end
