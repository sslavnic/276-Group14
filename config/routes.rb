Rails.application.routes.draw do

  get 'static_pages/home'

  

  resources :clubs
  resources :students
  resources :sessions
  resources :events

#  root			        	'students#new'
 # get    'home'   =>  'static_pages#home'
#  get    'login'   => 'sessions#new'
#  post   'login'   => 'sessions#create'
#  delete 'logout'  => 'sessions#destroy'
  
#  get     'error' => 'students#error'



#resources :students
#resources :sessions
  resources :account_activations, only: [:edit]
  root 'static_pages#home'
  #root  'students#new'
  get   'contact' => 'static_pages#contact'
  get   'signup'  => 'students#new'
  get    'login'	=> 'sessions#new'
  post   'login'	=> 'sessions#create'
  delete 'logout'	=> 'sessions#destroy'
  get    'error'	=> 'students#error'
  get	 'myClubs'	=> 'clubs#list'
  get	 'allClubs'	=> 'clubs#index'
  get 'browseByCategory' => 'clubs#categories'
  post 'join'     => 'clubs#join'
  delete  'event' =>  'events#destroy'
  get    'signUp' => 'events#sign_up'
  get    'drop' => 'events#drop'
  get   'student_events' => 'events#student_events'
  
  
  # call back route from Google
  get "/auth/:provider/callback" => "sessions#access"

end


# comments are from the merge