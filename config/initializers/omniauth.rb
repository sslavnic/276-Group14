Rails.application.config.middleware.use OmniAuth::Builder do
  provider :google_oauth2, ENV['GOOGLE_CLIENT_ID'], ENV['GOOGLE_SECRET'], {
    access_type: 'offline',
    scope: 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/calendar',
    #redirect_uri:'https://sfunite-mvarela.c9users.io/auth/google_oauth2/callback'
   #redirect_uri:'https://c276-laylat2.c9users.io/auth/google_oauth2/callback'
    redirect_uri:'https://sfunite.herokuapp.com/auth/google_oauth2/callback' 
  }
end