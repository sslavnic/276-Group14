require 'test_helper'

class StudentTest < ActiveSupport::TestCase
  

  def setup
    @student = Student.new(name: "dave", email: "dave@sfu.ca", password: "pass123", 
      password_confirmation: "pass123", studenttype: "Student", activated: true)
    @admin = Student.new(name: "adminDave", email: "adminDave@sfu.ca", password: "pass123",
      password_confirmation: "pass123", studenttype: "Club Admin", activated: true)
  end

  test "should be valid" do
    assert @admin.valid?
  end

  test "name should not be too long" do
    @student.name = "a" * 51
    assert_not @student.valid?
  end

  test "email should be present" do
    @student.email = ""
    assert_not @student.valid?
  end

  test "email should not be too long" do
    @student.email = "a" * 256
    assert_not @student.valid?
  end

  test "email should accept valid addresses" do
    valid_addresses = %w[tester@sfu.com other@gmail.com]
    valid_addresses.each do |v_a|
      @student.email = v_a
      assert @student.valid?, "#{v_a.inspect} should be valid"
    end
  end

  # Currently suspended
  # test "email validation should reject invalid addresses" do
  #   invalid_addresses = %w[something@hotmail.com user.org]
  #   invalid_addresses.each do |i_a|
  #     @student.email = i_a
  #     assert_not @student.valid?, "#{i_a.inspect} should be invalid"
  #   end
  # end

  test "email addresses should be unique" do
    dup_student = @student.dup
    dup_student.email = @student.email.upcase
    @student.save
    assert_not dup_student.valid?
  end

  test "email addresses should be saved as lower-case" do
    non_downcase_email = "Test@Example.com"
    @student.email = non_downcase_email
    @student.save
    assert_equal non_downcase_email.downcase, @student.reload.email
  end

  test "password should be present" do
    @student.password = @student.password_confirmation = " " * 6
    assert_not @student.valid?
  end

  test "password should have minimum length" do
    @student.password = @student.password_confirmation = "a" * 4
    assert_not @student.valid?
  end

  test "password should contain a letter and a number" do
    assert(@student.password =~ /[A-Z]/i)
    assert(@student.password =~ /\d/)
  end  

  # also currently suspended
  # test "authenticated? should return false for a user with nil digest" do
  #   assert_not @student.authenticated?('')
  # end

  test "studenttype must be either Student or Club Admin" do
    assert_equal @student.studenttype, "Student"
    assert_equal @admin.studenttype, "Club Admin"
  end


end
