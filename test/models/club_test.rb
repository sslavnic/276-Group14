require 'test_helper'
class ClubTest < ActiveSupport::TestCase
  
	def setup
		@club = clubs(:testClub)
		@student = students(:testStudent)
    	@admin = students(:testAdmin)
	end

	test "should be valid" do
		assert @club.valid?
	end

	test "club name can't be empty" do
		@club.name = ''
		assert_not @club.valid?
	end

	test "website name must include . seperator" do
		@club.website = "testclub.com"
		assert @club.valid?
	end

end

