require 'test_helper'

class StudentMailerTest < ActionMailer::TestCase
 
 
  test "account_activation" do
    student = students(:testStudent)
    student.activation_token = Student.new_token
    mail = StudentMailer.account_activation(student)
    assert_equal "Account activation", mail.subject
    assert_equal [student.email], mail.to
    assert_equal ["noreply@example.com"], mail.from
    assert_match student.name,               mail.body.encoded
    assert_match student.activation_token,   mail.body.encoded
    assert_match CGI::escape(student.email), mail.body.encoded
  end

end