# Preview all emails at http://localhost:3000/rails/mailers/student_mailer
class StudentMailerPreview < ActionMailer::Preview

  # Preview this email at https://sfunite-mvarela.c9users.io/rails/mailers/student_mailer/account_activation
  def account_activation
    student = Student.first
    student.activation_token = Student.new_token
    StudentMailer.account_activation(student)
  end

  # Preview this email at https://sfunite-mvarela.c9users.io/rails/mailers/student_mailer/passowrd_reset
  def password_reset
    StudentMailer.password_reset
  end

end
