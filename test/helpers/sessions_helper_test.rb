require 'test_helper'

class SessionsHelperTest < ActionView::TestCase

	test 'session' do
		log_in students(:testStudent)
		assert logged_in?
		assert_equal students(:testStudent).id, current_student.id
		log_out
		assert_not logged_in?
	end

end