module SessionsHelper

  def log_in(student)
    session[:student_id] = student.id
  end

  def remember(student)
    student.remember
    cookies.permanent.signed[:student_id] = student.id
    cookies.permanent[:remember_token] = student.remember_token
  end

  def current_student
    @current_student = Student.find_by(id: session[:student_id])
  end

  def is_student?
     @current_student.studenttype == 'Student'
  end

  def logged_in?
    !current_student.nil?
  end

  def admin_clubs
    current_student.clubs.where("admin_id = #{current_student.id}")
  end
  
  def log_out
    session.delete(:student_id)
    @current_student = nil
  end
  
  def access_granted?
    if current_student.token != nil
      current_student.token.access_token != nil
    end
  end
    
  
end