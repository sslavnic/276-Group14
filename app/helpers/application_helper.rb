module ApplicationHelper

  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "SFUnite"
    if page_title.empty?
      base_title
    else
      base_title + " | " + page_title
    end
  end
  
  def format_date(date)
    date.strftime("%d/%m/%Y at %H:%M:%S")
  end
  
  def embed_link(user)
    @escaped_email = CGI::escape("#{user.google_account}")
    @link = "https://calendar.google.com/calendar/embed?src=#{@escaped_email}&ctz=America/Vancouver"
  end
  
def parsed_tweet tweet
  _parsed_tweet = tweet.text.dup
  
tweet.urls.each do |urlLink|
  htmlLink = link_to(urlLink.display_url.to_s, urlLink.expanded_url.to_s, target: '_blank')
  _parsed_tweet.sub!(urlLink.url.to_s, htmlLink)  
end  

tweet.media.each do |urlLink|
  htmlLink = link_to(urlLink.display_url.to_s, urlLink.expanded_url.to_s, target: '_blank')
  _parsed_tweet.sub!(urlLink.url.to_s, htmlLink)  
end  

  _parsed_tweet.html_safe
end


end