module EventsHelper
	
	def convert_time(time)
		# Format to YYYY-MM-DDTHH:MM:SS-07:00
		# e.g. 2016-08-01T12:00:00-07:00
		formatted_time = "#{time.year}-#{time.month}-#{time.day}T#{time.hour}:#{time.min}:#{time.sec}-07:00"
	end
	
	def hash_event(event)
		start_time = event[:start_time]
		end_time = event[:end_time]
		event_hash = {summary: event[:name],
			location: event[:place],
			start: {dateTime: convert_time(start_time)},
			end: {dateTime: convert_time(end_time)},
			description: event[:description]
		}
	end
end
