module ClubsHelper
	
	def access_granted_for_club?(club)
		if club.token != nil
			club.token.access_token != nil
		end
	end
end
