class StudentMailer < ApplicationMailer

  def account_activation(student)
    @student = student
    mail to: student.email, subject: "Account activation"
  end
  
  # future implementation
  def password_reset
    @greeting = "Hi"

    mail to: "to@example.org"
  end
end
