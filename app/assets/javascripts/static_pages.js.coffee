# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

stop = false
$(document).ready ->
  tabCarousel = setInterval((->
    tabs = $('#tabs-home .nav.tabs > li')
    active = tabs.filter('.active')
    next = active.next('li')
    toClick = if next.length then next.find('a') else tabs.eq(0).find('a')
    toClick.trigger 'click'
    return
  ), 5000)
  return

