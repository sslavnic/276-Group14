class EventsController < ApplicationController
	include ClubsHelper
	include EventsHelper
	include SessionsHelper

	def new
		@event = Event.new
		@club = Club.find_by(:name => params[:name])
	end

	def show
		@event = Event.find_by(:id => params[:id])
		@club = @event.club
	end
	
	def student_events
		@student = current_student
		@events = @student.events
	end

	def index
		@club = Club.find_by(:name => params[:name])
		@events = @club.events
	end

	def create
		club = Club.find_by(:name => params[:name])
		@event = Event.new(event_params)
		if !@event.save
			flash[:error] = 'Could not save event.'
			render 'new'
		else
			club.events << @event
			if access_granted_for_club?(club)
				calendar = Googlecalendarwrapper.new(club)
				@output = calendar.insert_event(hash_event(@event))
				@event.update_attribute(:google_id, @output["id"])
				flash[:message] = "Saved Event to Calendar"
			end
			redirect_to club
		end
	end
	
	def sign_up
		@event = Event.find_by(:id => params[:id])
		@student = Student.find_by(:name => params[:student])
		@student.events << @event
		if @student.google_account != nil
			calendar = Googlecalendarwrapper.new(@student)
			@output = calendar.insert_event(hash_event(@event))
			@student.update_attribute(:google_event_id, @output["id"])
			flash[:message] = "Saved Event to Calendar"
		end
		redirect_to current_student
	end
	
	def drop
		@event = Event.find_by(:id => params[:id])
		@student = Student.find_by(:name => params[:student])
		if @student[:google_event_id] != nil
			calendar = Googlecalendarwrapper.new(@student)
			calendar.delete_event(@student[:google_event_id])
			flash[:message] = "Deleted event from calendar."
		end
		@student.events.delete(@event)
		redirect_to current_student
	end

	def destroy
		@event = Event.find_by(:id => params[:id])
		club = @event.club
		
		if @event[:google_id] != nil
			calendar = Googlecalendarwrapper.new(club)
			calendar.delete_event(@event[:google_id])
			flash[:message] = "Deleted event from calendar."
		end
		@event.destroy
		redirect_to club
	end
	
	private
		def event_params
			params.require(:event).permit(:name, :description, :start_time, :end_time, :place)
		end
end
