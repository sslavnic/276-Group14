class SessionsController < ApplicationController
include SessionsHelper

  def new
  end



  def create
    student = Student.find_by(email: params[:session][:email].downcase)
    if student && student.authenticate(params[:session][:password])
      if student.activated?
        log_in student
        redirect_to student   #student
      else
        message  = "Account not activated. "
        message += "Check your email for the activation link."
        flash[:warning] = message
        redirect_to root_url
      end
    else
      flash[:danger] = 'Invalid email/password combination'
      redirect_to root_url    # render "new"
      #log_in student  

    end
  end
  
  def access
    #What data comes back from OmniAuth?     
    @auth = request.env["omniauth.auth"]
    @params = request.env["omniauth.params"]
    # Save tokens for current user
    if @params == {} # Credentials for student
      current_student.update_attribute(:google_account, @auth["info"]["email"])
      current_student.create_token(:access_token => @auth["credentials"]["token"],
                            :refresh_token => @auth["credentials"]["refresh_token"],
                            :expires_at => Time.at(@auth["credentials"]["expires_at"]).to_datetime)
      
      redirect_to current_student
    else # Params hash non-empty, credentials for club
      club = Club.find_by(:name => @params["name"])
      club.update_attribute(:google_account, @auth["info"]["email"])
      club.create_token(:access_token => @auth["credentials"]["token"],
                        :refresh_token => @auth["credentials"]["refresh_token"],
                        :expires_at => Time.at(@auth["credentials"]["expires_at"]).to_datetime)
      
      redirect_to club
    end
  end

  def destroy
    log_out 
    redirect_to root_url
  end
  
  
end