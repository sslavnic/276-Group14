class ClubsController < ApplicationController
	
	#The club form
	def new
		@club = Club.new
	end
	
	#Display the info for a single club
	def show
		@club = Club.find(params[:id])
	end

	#Join the club whose page you're on
	def join
		@club = Club.find(params[:id])
		@club.students << current_student
		redirect_to myClubs_url
	end

	#List all clubs in model
	def index
		@clubs = Club.all.order(:name)
		if params[:search]
    		@clubs = Club.search(params[:search]).order(:name)
		else
    		@clubs = Club.all.order(:name)
 		end
	end

	def categories
		@clubs = Club.all.order(:name)
	end
	
	#List all clubs that the current student is a member/admin of
	def list
		@clubs = current_student.clubs
	end

	#Save the club to the model and update join relation
	def create
		#First save the new club
	    @club = Club.new(club_params)
	    @club.admin_id = current_student.id
	    if !@club.save
	      redirect_to current_student
	    else
	    #Then record the relationship between the creator and the club
	    @club.students << current_student
	    redirect_to myClubs_url
	    end
  	end
	
		
	
	def edit
		@club = Club.find(params[:id])
	end
	
	
	def update
		@club = Club.find(params[:id])
		if @club.update_attributes(club_params)
			redirect_to @club
		else
			render 'edit'
		end
	end
	
	
	
	#No option to do this yet
	def destroy

	end

	private

		def club_params
   			params.require(:club).permit(:name, :description, :website, :insta, :twitt, :choice1, :choice2, :choice3, :choice4, :choice5, :choice6, :choice7, :choice8, :choice9, :choice10,:SunDesc, 	:MonDesc, 	:TueDesc, 	:WedDesc, 	:ThuDesc, 	:FriDesc, 	:SatDesc, 	:SunTime, 	:MonTime, 	:TueTime, 	:WedTime, 	:ThuTime, 	:FriTime, 	:SatTime, 	:SunPlace, 	:MonPlace, 	:TuePlace, 	:WedPlace, 	:ThuPlace, 	:FriPlace, 	:SatPlace)
  			#params.require(:club).permit(:name, :description, :website, :choice1, :choice2, :choice3, :choice4, :choice5, :choice6, :choice7, :choice8, :choice9, :choice10,:SunDesc, 	:MonDesc, 	:TueDesc, 	:WedDesc, 	:ThuDesc, 	:FriDesc, 	:SatDesc, 	:SunTime, 	:MonTime, 	:TueTime, 	:WedTime, 	:ThuTime, 	:FriTime, 	:SatTime, 	:SunPlace, 	:MonPlace, 	:TuePlace, 	:WedPlace, 	:ThuPlace, 	:FriPlace, 	:SatPlace)
  		end
end
