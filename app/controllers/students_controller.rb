class StudentsController < ApplicationController
    before_action :logged_in_student, only: [:show]
    before_action :current_logged_in_student,   only: [:show]

  def show
    @student = Student.find(params[:id])
    redirect_to root_url and return unless @student.activated?
  end

  def new
    @student = Student.new
  end
  
  def create
    @student = Student.new(student_params)
    @student.activated = true
    if @student.save
      StudentMailer.account_activation(@student).deliver_now
      flash[:info] = "Please check your email to activate your account."
      redirect_to root_url
    else
      render 'new'
    end
  end
  
private
  def student_params
   params.require(:student).permit(:studenttype, :name, :email, :password, :password_confirmation)
  end
    
  def logged_in_student
    unless logged_in?
        redirect_to root_url
    end
  end
  
  def current_logged_in_student
      @student = Student.find(params[:id])
      redirect_to error_path unless @student == current_student
  end
end

