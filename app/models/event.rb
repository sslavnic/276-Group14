class Event < ActiveRecord::Base
	belongs_to :club
	belongs_to :student
	
	validates	:name, presence: true
	validate :start_and_end_times
	
	def start_and_end_times
	  if self.start_time && (self.start_time > self.end_time)
	    self.errors.add(:start_time, "End time must be greater than start time.")
	  end
	end
end
