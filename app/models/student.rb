class Student < ActiveRecord::Base
  
  attr_accessor :activation_token
  before_save :downcase_email
  before_create :create_activation_digest
  has_secure_password
  
  has_and_belongs_to_many :clubs
  has_many :events#, :through => :clubs
  has_one :token
  
  before_save { email.downcase! }
  
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  validates :email, presence: true, uniqueness: { case_sensitive: false, message: "already in use" }, format: { with: VALID_EMAIL_REGEX }, length: { maximum: 255, message: "email was too long ( must be less than 256 chars )" }
  validates :name, presence: true, length: { maximum: 40, message: "was too long ( max 40 characters )" }
  validates :password, presence: true, length: { minimum: 4, message: "minimum length is 4 characters" }
  validate  :valid_password_complexity?
  
  # Returns the hash digest of the given string.
  def Student.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  
  # Returns a random token.
  def Student.new_token
    SecureRandom.urlsafe_base64
  end
  
  # Returns true if the given token matches the digest.
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end
  
  private
    # Converts email to all lower-case.
    def downcase_email
      self.email = email.downcase
    end

    # Creates and assigns the activation token and digest.
    def create_activation_digest
      self.activation_token  = Student.new_token
      self.activation_digest = Student.digest(activation_token)
    end

    #Check that password fits complexity standard (1 letter & 1 num)
    def valid_password_complexity?
      if not self.password =~ /[a-z]/i 
        errors.add(:password, "must contain at least 1 letter")
        return false
      end
      if not self.password =~ /\d/
        errors.add(:password, "must contain at least 1 number")
        return false
      end
      return true
    end

end
