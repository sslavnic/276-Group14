class Club < ActiveRecord::Base
	has_and_belongs_to_many :students
	has_many :events

	has_one :token
	
  validates :name, presence: true, uniqueness: { case_sensitive: false, message: "is already taken by another club" }, length: { minimum: 3, message: "has to be at least 3 letters long"}
  validates :description, presence: true, length: { maximum: 300, message: "is too long"}
 

  validates :website, presence: false, allow_blank: true, allow_nil: true
 
  validates :insta, presence: false, allow_blank: true, allow_nil: true
   
  validates :twitt, presence: false, allow_blank: true, allow_nil: true 
 
  validates :SunDesc, presence: false, allow_blank: true, allow_nil: true
  validates :MonDesc, presence: false, allow_blank: true, allow_nil: true
  validates :TueDesc, presence: false, allow_blank: true, allow_nil: true
  validates :WedDesc, presence: false, allow_blank: true, allow_nil: true
  validates :ThuDesc, presence: false, allow_blank: true, allow_nil: true
  validates :FriDesc, presence: false, allow_blank: true, allow_nil: true
  validates :SatDesc, presence: false, allow_blank: true, allow_nil: true
  validates :SunTime, presence: false, allow_blank: true, allow_nil: true
  validates :MonTime, presence: false, allow_blank: true, allow_nil: true
  validates :TueTime, presence: false, allow_blank: true, allow_nil: true
  validates :WedTime, presence: false, allow_blank: true, allow_nil: true
  validates :ThuTime, presence: false, allow_blank: true, allow_nil: true
  validates :FriTime, presence: false, allow_blank: true, allow_nil: true
  validates :SatTime, presence: false, allow_blank: true, allow_nil: true
  validates :SunPlace, presence: false, allow_blank: true, allow_nil: true
  validates :MonPlace, presence: false, allow_blank: true, allow_nil: true
  validates :TuePlace, presence: false, allow_blank: true, allow_nil: true
  validates :WedPlace, presence: false, allow_blank: true, allow_nil: true
  validates :ThuPlace, presence: false, allow_blank: true, allow_nil: true
  validates :FriPlace, presence: false, allow_blank: true, allow_nil: true
  validates :SatPlace, presence: false, allow_blank: true, allow_nil: true

def self.search(search)
  #iLIKE is used for postgres
  
  #where("name iLIKE ?", "%#{search}%")
  
  #search in both name and club description
  where("name iLIKE ? OR description iLIKE ?", "%#{search}%", "%#{search}%")
  
end
  
  
end
