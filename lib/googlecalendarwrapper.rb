class Googlecalendarwrapper
  
  def initialize(user)
    configure_client(user)
    configure_calendar
  end
	  
  def configure_client(user)
    @client = Google::APIClient.new
    @client.authorization.client_secret = ENV['GOOGLE_SECRET']
    @client.authorization.client_id = ENV['GOOGLE_CLIENT_ID']
    @client.authorization.refresh_token = user.token.refresh_token
    @client.authorization.access_token = user.token.access_token
    @client.authorization.refresh!
  end
  
  def configure_calendar
    @calendar = @client.discovered_api('calendar', 'v3')
  end
  
  def get_calendars
    response = @client.execute(api_method: @calendar.calendar_list.list)
    @list = JSON.parse(response.body)
  end
  
  def get_events_busy?(start_time, end_time)
    @client.execute(api_method: @calendar.freebusy.query, 
                    body: JSON.dump({timeMin: start_time,
                    timeMax: end_time,
                    items: ['primary']}),
                    headers: {'Content-Type' => 'application/json'})
  end
  
  def insert_event(event)
    # event objects should be passed in as follows:
    # event = {summary: "Let's Check Out Some Fish Again", 
    #       location: "Vancouver Aquarium",
    #       start: {dateTime: '2016-08-03T14:00:00-0700'},  
    #       end: {dateTime: '2016-08-03T16:00:00-0700'},  
    #       description: "Testing insert even from rails with corrected time zones"}
    response = @client.execute(:api_method => @calendar.events.insert,
                             :parameters => {'calendarId' => 'primary','sendNotifications' => true},
                             :body => JSON.dump(event),
                             :headers => {'Content-Type' => 'application/json'})
    JSON.parse(response.body)
  end
  
  def delete_event(event_id)
    @client.execute(:api_method => @calendar.events.delete,
                    :parameters => {'calendarId' => 'primary', 'eventId' => event_id})
  end
  
end