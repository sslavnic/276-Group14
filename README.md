<<<<<<< HEAD
# ruby-getting-started

A barebones Rails app, which can easily be deployed to Heroku.

This application support the [Getting Started with Ruby on Heroku](https://devcenter.heroku.com/articles/getting-started-with-ruby) article - check it out.

## Running Locally

Make sure you have Ruby installed.  Also, install the [Heroku Toolbelt](https://toolbelt.heroku.com/).

```sh
$ git clone git@github.com:heroku/ruby-getting-started.git
$ cd ruby-getting-started
$ bundle install
$ bundle exec rake db:create db:migrate
$ heroku local
```

Your app should now be running on [localhost:5000](http://localhost:5000/).

## Deploying to Heroku

```sh
$ heroku create
$ git push heroku master
$ heroku run rake db:migrate
$ heroku open
```

or

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

## Docker

The app can be run and tested using the [Heroku Docker CLI plugin](https://devcenter.heroku.com/articles/introduction-local-development-with-docker).

Make sure the plugin is installed:

    heroku plugins:install heroku-docker

Configure Docker and Docker Compose:

    heroku docker:init

And run the app locally:

    docker-compose up web

The app will now be available on the Docker daemon IP on port 8080.

To work with the local database and do migrations, you can open a shell:

    docker-compose run shell
    bundle exec rake db:migrate

You can also use Docker to release to Heroku:

    heroku create
    heroku docker:release
    heroku open

## Documentation

For more information about using Ruby on Heroku, see these Dev Center articles:

- [Ruby on Heroku](https://devcenter.heroku.com/categories/ruby)

=======
WEBAPP NAME :SFUnite
GIT LINK: git@gitlab.com:sslavnic/276-Group14.git

ABSTRACT
SFUnite is a web application with the goal of increasing engagement at SFU by helping students connect with clubs and associations on campus. Currently, SFU holds a clubs day event on campus at the beginning of each semester where each club sets up a booth and tries to recruit new members. SFUnite would act as a virtual clubs day to make it easy and efficient for students and clubs alike to connect throughout the year. The Student Society also has a list of clubs on campus and their websites but the SFUnite application would bring these sources together into a single platform. Furthermore, an online platform allows for a customizable experience for each student that simply cannot be achieved in person.

PROBLEM
SFU students wanting to sign up for a club or access information are required to wade through a patchwork of social media sites and/or use the Student Society club listing which only links to these various sites.  For instance, students may be interested in upcoming events for two clubs; one club provides its information via Twitter while the other uses Facebook. Students would have to continuously visit different sites for updates and manually check their schedules for conflicts in meeting times/ events. This an inefficient way for tracking club information and signing up for clubs/events. SFUnite provides one location for all clubs and its members to connect. Students will be able to view upcoming events, meeting times and important information of all clubs. It will also allow students an easy way to sign up for clubs/events based on their interests and schedules .
	 	 	
SCOPE

Features

The web app will feature the following: login, scheduling, recommendations, a club database, events/ news feed, club pages and feedback/rating. Login will allow two types of users: Students and Club Owners. Users will need to provide a SFU email in order to create an account. With scheduling, they will see a calendar that will help visualize which club’s meeting times and events fit their own schedule for that semester. Elementary machine learning will be implemented to recommend clubs of interest to each user. The club database will allow users to find information about the different clubs from a single source. Upcoming events will be available in a feed that includes the information about the event, the next public transit to the event and the option to RSVP. Every club will have their own personal page where they will have general information and feed/activity from different social networking site including, but not limited to, Instagram, Twitter and Facebook. Clubs will also have an activity meter showing how active the club is while students will be able to give feedback about the clubs or a specific event in an anonymous mode or user mode. 

Story 1:
A new student user signs up for SFUnite using an SFU email address. After the sign up process, the user sees a list of recommended clubs based on his/her interests as well as a calendar showing his/her semester schedule. Using these two features, the user signs up to be a member of club X.
Story 2:
A student user signs into his/her account to get an update on current events for clubs. In the events feed, the user sees an upcoming event X that is interesting, so the user RSVPs. However, a conflict shows up in his/her calendar showing the time overlap of this event and a class. The user chooses to keep the RSVP.
Story 3:
An admin user signs into SFUnite to update club information. The admin user creates a new event for their club providing the location and time as well as a description of the event. The events feed now shows this event as well as public transit information relating to the location and time of the event.

Audience
For the class project , the audience for the webapp is SFU clubs and students, but it can be implemented for different schools or city clubs.


Work Required
Between creating a nice interface, setting up the club and student database, configuring a scheduling app and using machine learning to recommend clubs, we believe this project is doable but will keep all five group-members busy.
>>>>>>> e00f17cd5e4d3b0cec3c7bf6f0365d6576988fd4
